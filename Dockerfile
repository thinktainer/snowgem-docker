# BUILD CONTAINER
FROM ubuntu:xenial as builder

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
	build-essential libtool autotools-dev automake pkg-config \
	zlib1g-dev libz-dev libevent-dev libzstd-dev git \
	libcap-dev libminiupnpc-dev curl

ENV coin=snowgem repo=snowgem
ENV repo_url=https://github.com/${coin}/${repo}.git
RUN mkdir -p /build
WORKDIR /build
RUN git clone --branch master --single-branch --recurse-submodules ${repo_url} ${repo}
RUN apt-get install -y cmake python && cd /build/${repo}/depends && chmod +x ./config.guess && chmod +x ./config.sub && make NO_RUST=1
WORKDIR /build/${repo}
RUN apt-get install -y bsdmainutils
RUN chmod +x ./autogen.sh && ./autogen.sh
RUN export BUILD=$(./depends/config.guess) && ./configure --prefix="${PWD}/depends/${BUILD}" --host=$BUILD --disable-rust --disable-mining --disable-test
RUN chmod +x ./share/genbuild.sh && chmod +x ./src/leveldb/build_detect_platform && make -j `nproc --ignore=4`


# RUN CONTAINER
FROM ubuntu:xenial

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y libgomp1 wget
WORKDIR /root/
COPY --from=builder /build/snowgem/contrib/debian/examples/snowgem.conf /root/example.conf
COPY --from=builder /build/snowgem/src/snowgemd /usr/bin/
COPY --from=builder /build/snowgem/src/snowgem-cli /usr/bin/
COPY --from=builder /build/snowgem/src/snowgem-tx /usr/bin/
COPY --from=builder /build/snowgem/zcutil/fetch-params.sh /usr/bin/
ADD https://snowgem.org/downloads/sprout-proving.key /root/.snowgem-params/sprout-proving.key.dl
RUN chmod +x /usr/bin/fetch-params.sh
RUN /usr/bin/fetch-params.sh
ENTRYPOINT ["/bin/bash"]
